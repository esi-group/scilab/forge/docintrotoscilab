// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// -1. The input argument of a function can be a value or a variable,
// which does not change its behavior.
clear
function y = function1 ( x )
  y = 2 * x
endfunction
y = function1 ( 2 )
x = 2;
y = function1 ( x )

// 0. The name of the output argument has no influence : it is a dummy argument.
clear
function y = function1 ( x )
  y = 2 * x
endfunction
z = function1 ( 2 )
// The variable y does not exist.
y

// 1. Variables created inside the function does not exist outside.
clear
function y = function1 ( x )
  a = 2 * x
  y = a
endfunction
y = function1 ( 2 )
a

// 2. Input arguments can be modified without modifying the variable in the caller's context.
clear
function y = function2 ( x )
  x = 2 * x
  y = x
endfunction
x = 2;
y = function2 ( x )
x
// x has not changed.

// 3. Variables created at level #k are available at level #k+1.
// (This is a feature which should be used only sparingly.)
clear
function y = function3 ( x )
  a = 2 * x
  y = 3 * function4 ( x )
endfunction
function y = function4 ( x )
  disp(a)
  y = 3 * a
endfunction
y = function3 ( 3 )

