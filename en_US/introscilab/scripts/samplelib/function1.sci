// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = function1 ( x )
  y = 1 * function1_support ( x )
endfunction
function y = function1_support ( x )
  y = 3 * x
endfunction

