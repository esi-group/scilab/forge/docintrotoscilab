// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// configure title and axis
function f = myquadratic ( x )
  f = x^2
endfunction
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata )
h = gca();
h.title.text = "My title";
h.x_label.text = "X axis";
h.y_label.text = "Y axis";

// Configure legend with the legend function
function f = myquadratic ( x )
  f = x^2
endfunction
function f = myquadratic2 ( x )
  f = 2 * x^2
endfunction
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata , "+-" )
ydata2 = myquadratic2 ( xdata );
plot ( xdata , ydata2 , "o-" )
h = gca();
h.title.text = "My title";
h.x_label.text = "X axis";
h.y_label.text = "Y axis";
legend ( "x^2" , "2x^2" );

