// Copyright (C) 2009 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


////////////////////////////////////////////////////////
// Creating real variables
x=1
x = x * 2
// Using semicolon to disable the display of the content
y=1;
y=y*2;
////////////////////////////////////////////////////////
// Variable name : Scilab is case-sensitive
A = 2
a = 1
A
a
////////////////////////////////////////////////////////
// Comments and continuation lines
// This is my comment.
x=1..
+2..
+3..
+4

////////////////////////////////////////////////////////
// Elementary functions
x = cos(2)
y = sin(2)
x^2+y^2

////////////////////////////////////////////////////////
// Pre-defined mathematical variables
c=cos(%pi)
s=sin(%pi)
c^2+s^2

////////////////////////////////////////////////////////
// Booleans
a=%T
b = ( 0 == 1 )
a&b

////////////////////////////////////////////////////////
// Complex numbers
x= 1+%i
isreal(x)
x'
y=1-%i
real(y)
imag(y)

////////////////////////////////////////////////////////
// Strings
x = "foo"
y="bar"
x+y

////////////////////////////////////////////////////////
// Integer variables
i = uint32(0)
j=i-1
k = j+1
n=32
format(25)
2^n - 1

////////////////////////////////////////////////////////
// Integer values and real variables
n=2 // a double
n^1023
n = uint16(2) // an integer
n^1023

////////////////////////////////////////////////////////
// Dynamic type of variables
x=1
x+1
x="foo"
x+"bar"

////////////////////////////////////////////////////////
// using ans
exp(3)
t = log(ans)


////////////////////////////////////////////////////////
// Low-level operations
A = [1 2
3 4] 
B=[5 6
7 8]
A+B

A = [1 2
3 4]
A + 1 // Adds 1 on all elements

A = [1 2
3 4]
B = [1 2 3
4 5 6]
A+B // This is an error

////////////////////////////////////////////////////////
// Elementwise operations
A = ones(2,2)
B = 2 * ones(2,2)
A*B
A.*B

////////////////////////////////////////////////////////
// Conjugate transpose and non-conjugate transpose
A = [1 2;3 4] + %i * [5 6;7 8]
A'
A.'

B = [1 2;3 4]
B'
B.'

