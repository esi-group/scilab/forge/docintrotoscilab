// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


pwd
SCI
ls(SCI+"/modules")
ls(SCI+"/modules/graphics/demos")
exec(SCI+"/modules/graphics/demos/2d_3d_plots/contourf.dem.sce")

