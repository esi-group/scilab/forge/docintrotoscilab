// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// ------------------------------------------------------
curdir = pwd();
cleaner_path = get_file_path('cleaner.sce');
chdir(cleaner_path);
// ------------------------------------------------------
for ext = ["*.bin" "lib" "names"]
  fl = ls(ext);
  if fl <> [] then
    for fn = fl'
      mprintf("Deleting %s\n",fn)
      mdelete(fn);
    end
  end
end
// ------------------------------------------------------
chdir(curdir);
// ------------------------------------------------------
