// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Create the vector [x1+1 x2+1 x3+1 x4+1]
x = 1:4;
y = x + 1

// Create the vector [x1*y1 x2*y2 x3*y3 x4*y4]
x = 1:4;
y = 5:8;
z = x .* y

// Create the vector [1/x1 1/x2 1/x3 1/x4]
x = 1:4;
y = 1 ./ x 
y = 1 / x // False: now y is the solution of x*y = 1


// Create the vector [x1/y1 x2/y2 x3/y3 x4/*y4]
x = 12*(6:9);
y = 1:4;
z = x ./ y

// Create the vector [x1^2 x2^2 x3^2 x4^2]
x = 1:4;
y = x.^2

// Create the vector [sin(x1) sin(x2) sin(x3) sin(x4)]
x = linspace(0,%pi,10);
y = sin(x)

// Draw the function log10(r/10.^(x) + 10.^(x)) with r = 2.220D-16 for x\in[-16,0]
r = 2.220D-16;
x = linspace (-16,0,100);
y = log10(r./10.^x + 10.^x);
plot(x,y)

