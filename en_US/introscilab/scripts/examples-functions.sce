// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



////////////////////////////////////////////////////////
// Using a function
A = testmatrix("hilb",2)
[L,U] = lu(A)
[L,U,P] = lu(A)

////////////////////////////////////////////////////////
// Defining a function
function y = myfunction ( x )
  y = 2 * x
endfunction

// We must set the variable y
function y = myfunction ( x )
  z = 2 * x
endfunction
myfunction ( 1 )

////////////////////////////////////////////////////////
// Managing output arguments
function [y1 , y2] = simplef ( x1, x2 )
  y1 = 2 * x1
  y2 = 3 * x2
endfunction

function y = fmain ( x )
  y = 2 * flevel1 ( x )
endfunction

function y = flevel1 ( x )
  y = 2 * flevel2 ( x )
endfunction

function y = flevel2 ( x )
  y = 2 * x
  whereami()
endfunction

// Using return
function y = mysum ( istart , iend )
  if ( istart < 0 ) then
    y = 0
    return
  end
  if ( iend < istart ) then
    y = 0
    return
  end
  y = sum ( istart : iend )
endfunction

// A bugged function
function y = mysum ( istart , iend )
  y = sum ( iend : istart , "foo" )
endfunction

