// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// A naive average
function y = mynaivemean ( v )
  y = 0;
  n = length(v);
  for i = 1:n
    y = y + v(i);
  end
  y = y / n;
endfunction

n_data = logspace(1,6,6);
for i = 1:6
  n = n_data(i);
  v = rand(n,1);
  timer();
  ymean = mynaivemean ( v );
  t = timer();
  mprintf ("i=%d, N=%d, mean=%f, t=%f\n", i , n , ymean , t );
end

// Using mean
n_data = logspace(1,6,6);
for i = 1:6
  n = n_data(i);
  v = rand(n,1);
  timer();
  ymean = mean ( v );
  t = timer();
  mprintf ("i=%d, N=%d, mean=%f, t=%f\n", i , n , ymean , t );
end

// A fast average
function y = myfastmean ( v )
  y = sum(v);
  n = length(v);
  y = y / n;
endfunction
n_data = logspace(1,6,6);
for i = 1:6
  n = n_data(i);
  v = rand(n,1);
  timer();
  ymean = myfastmean ( v );
  t = timer();
  mprintf ("i=%d, N=%d, mean=%f, t=%f\n", i , n , ymean , t );
end

