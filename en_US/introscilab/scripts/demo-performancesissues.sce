// Copyright (C) 2009 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


////////////////////////////////////////////////////////
// Dynamic matrices is not good for performances
// We want to create a 700 x 700 matrix with a formula.
// Solution #1 : dynamic matrices
n=10000;
clear A
timer();
for i = 1 : n
  A(i)=1;
end
timer
// Solution #2 : we first initialize the matrix
clear A
timer();
A=zeros(n);
for i = 1 : n
  A(i)=1;
end
timer

// Result : not clear...

