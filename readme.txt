Introduction To Scilab

Abstract
--------

In this document, we make an overview of Scilab features so that we can 
get familiar with this environment as fast as possible. The goal is to 
present the core of skills necessary to start with Scilab. In the first 
part, we present how to get and install this software on our computer. 
We also present how to get some help with the provided in-line 
documentation and also thanks to web resources and forums. In the 
remaining sections, we present the Scilab language, especially its 
structured programming features. We present an important feature of 
Scilab, that is the management of real matrices and overview the linear 
algebra library. The definition of functions and the elementary 
management of input and output variables is presented. We present 
Scilab's graphical features and show how to create a 2D plot, how to 
configure the title and the legend and how to export that plot into a 
vectorial or bitmap format.

Author
------

 * Copyright (C) 2013 - Michael Baudin
 * Copyright (C) 2013 - Jerome Briot
 * Copyright (C) 2008-2011 - Consortium Scilab - Digiteo - Michael Baudin
 * Copyright (C) 2010 - Artem Glebov

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :
http://creativecommons.org/licenses/by-sa/3.0/

